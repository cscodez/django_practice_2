from django.http import Http404, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render


monthly_challenges = {
    "january": "Eat no meat for the entire month!",
    "february": "Walk for at least 20 minutes every day!",
    "march": "Learn Django for at least 20 minutes every day!",
    "april": "Sleep 8 hours every day!",
    "may": "Every day help somebody in something!",
    "june": "Do some physical education 3 days a week!",
    "july": "Call on your forgotten friends!",
    "august": "Smoke fewer cigarettes every day!",
    "september": "Tidy a room every week properly!",
    "october": "Don't drink alcohol this month!",
    "november": "Don't spend money for unnecessary things, but save money!",
    "december": None

}


# Create your views here.


def index(request):
    months = list(monthly_challenges.keys())

    return render(request, "challenges/index.html", {
        "months": months
    })


def monthly_challenge_by_number(request, month):
    months = list(monthly_challenges.keys())

    if month > len(months):
        return HttpResponseNotFound("<h1>Invalid month!</h1>")
    redirect_month = months[month - 1]
    redirect_path = reverse("month-challenge", args=[redirect_month])  # /challenge/january
    return HttpResponseRedirect(redirect_path)


def monthly_challenge(request, month):
    try:
        challenge_text = monthly_challenges[month]
        return render(request, "challenges/challenge.html", {
            "text": challenge_text,
            "month_name": month
        })
    except:
        raise Http404()
        # It looks for 404.html automatically in templates folder
        # Set DEBUG = False once you deploy, until we need it to be True
